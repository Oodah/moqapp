using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Moq;
using MOQapp;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace XUnitTest1
{
    public class BandControllerTest
    {
        [Fact]
        public void MethodTest()
        {
            //Arrange
            var listOfBands = new List<Band>
            {
                new Band
                {
                    Name = "Bayside",
                    Origin = "Queens, New York, USA",
                    Genre = "Alternative",
                    BestSong = "I and I"
                }
            };

            Mock<IRepository> mockRepository = new Mock<IRepository>();
            mockRepository.Setup(x => x.GetBands()).Returns(listOfBands);

            //Act
            var bandController = new BandController(mockRepository.Object);
            bandController.MethodTest();
            bandController.Should().NotBeNull();

        }

        [Fact]
        public void AddBand()
        {
            //Arrange 
            IQueryable<Band> bands =
                new List<Band>
                {
                    new Band{
                        Name = "Bayside",
                        Origin = "Queens, New York, USA",
                        Genre = "Alternative",
                        BestSong = "I and I"
                    }
                }.AsQueryable();

            var mockSet = new Mock<DbSet<Band>>();

            var mockContext = new Mock<MOQContext>();
            mockContext.Setup(x => x.Set<Band>()).Returns(mockSet.Object);

            //Acts
            var repository = new Repository(mockContext.Object);
            repository.AddBands();

            //Assert
           // mockSet.Verify(m => m.Add(It.IsAny<Band>()), Times.Once);
            //mockContext.Verify(m => m.SaveChanges(), Times.Once);
        }
    }
}
