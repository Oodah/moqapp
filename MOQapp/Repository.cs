﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOQapp
{
    public class Repository : IRepository
    {
        private MOQContext _context;

        public Repository(MOQContext context)
        {
            _context = context;
        }
        public List<Band> GetBands()
        {
            var  listOfBands = new List<Band>();
            listOfBands.Add(
                new Band
                {
                    Name = "Bayside",
                    Origin = "Queens, New York, USA",
                    Genre = "Alternative",
                    BestSong = "I and I"
                });

            return listOfBands;
        }

        public void AddBands()
        {
            _context.Bands.Add(
              new Band
              {
                  Name = "Bayside",
                  Origin = "Queens, New York, USA",
                  Genre = "Alternative",
                  BestSong = "I and I"
              });

            _context.SaveChanges();

        }
    }
}
