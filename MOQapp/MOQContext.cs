﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOQapp
{
    public class MOQContext : DbContext
    {
        public MOQContext()
        {}
        public MOQContext(DbContextOptions options) : base(options) { }
        public DbSet<Band> Bands { get; set; }
    }
}
