﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOQapp
{
    public class BandController
    {
        private IRepository _repository;
        private MOQContext _context;


        public BandController(IRepository repository)
        {
            this._repository = repository;
        }
       public void MethodTest()
        {
            var bands = this._repository.GetBands();
            foreach (var band in bands)
            {
                if (band.Name == "Bayside")
                {
                    System.Diagnostics.Debug.WriteLine("Band*******************************************");
                }

            }
        }
    }
}
