﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOQapp
{
    public interface IRepository
    {
        List<Band> GetBands();
    }
}
