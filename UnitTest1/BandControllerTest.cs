﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace UnitTest1
{
    [TestClass]
    public class BandControllerTest
    {
        [TestMethod]
        public void MethodTest()
        {
            var listOfBands = new List<Band>
            {
                new Band
                {
                    Name = "Bayside",
                    Origin = "Queens, New York, USA",
                    Genre = "Alternative",
                    BestSong = "I and I"
                }
            };

            Mock<IRepository> mockRepository = new Mock<IRepository>();
            mockRepository.Setup(x => x.GetBands()).Return(listOfBands);
            var bandController = new BandController(mockRepository.Object);
            bandController.MethodToTest();
            bandController.Should().NotBeNull();
        }
    }
}
